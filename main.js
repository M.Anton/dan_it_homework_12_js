"use strict"

let buttons = document.querySelectorAll('.btn-wrapper .btn');

document.addEventListener('keydown', function(event){

    for (let button of buttons) {
        if (event.code === 'Enter' && button.textContent === "Enter"){
            for (let btn of buttons) {
                btn.style.backgroundColor = 'black';
            }
            button.style.backgroundColor = 'blue'; 
        }
        if (event.code === 'KeyS' && button.textContent === "S"){
            for (let btn of buttons) {
                btn.style.backgroundColor = 'black';
            }
            button.style.backgroundColor = 'blue'; 
        }
        if (event.code === 'KeyE' && button.textContent === "E"){
            for (let btn of buttons) {
                btn.style.backgroundColor = 'black';
            }
            button.style.backgroundColor = 'blue'; 
        }
        if (event.code === 'KeyO' && button.textContent === "O"){
            for (let btn of buttons) {
                btn.style.backgroundColor = 'black';
            }
            button.style.backgroundColor = 'blue'; 
        }
        if (event.code === 'KeyN' && button.textContent === "N"){
            for (let btn of buttons) {
                btn.style.backgroundColor = 'black';
            }
            button.style.backgroundColor = 'blue'; 
        }
        if (event.code === 'KeyL' && button.textContent === "L"){
            for (let btn of buttons) {
                btn.style.backgroundColor = 'black';
            }
            button.style.backgroundColor = 'blue'; 
        }
        if (event.code === 'KeyZ' && button.textContent === "Z"){
            for (let btn of buttons) {
                btn.style.backgroundColor = 'black';
            }
            button.style.backgroundColor = 'blue'; 
        }
    }
})